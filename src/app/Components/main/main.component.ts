import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { OperacionesPokemonService } from './../../Services/operaciones-pokemon.service';
import { interfaceDatosPokemon, interfaceBusquedaPokemon } from './../../Interfaces/datos-pokemon';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  _datosPokemonInterface: interfaceDatosPokemon = {
    id: '',
    name: '',
    image: '',
    attack: '',
    defense: '',
    hp: '41',
    type: 'Planta',
    idAuthor: '1',
  };
  _busquedaPokemonInterface:interfaceBusquedaPokemon = {
    buscarPokemon: ""
  };
  dataPokemon: any = [];
  flagFormNuevo = false;
  flagEditar = false;
  idEditar = "";
  mensajeEvento= "";

  public formDatosPokemon: FormGroup;
  public formBuscarPokemon: FormGroup;

  constructor(private cargarPokemones: OperacionesPokemonService,
    private _operacionPokemon: OperacionesPokemonService) {
      this.formDatosPokemon = new FormGroup({
        name: new FormControl('', [Validators.required]),
        image: new FormControl('', [Validators.required]),
        attack: new FormControl('', [Validators.required]),
        defense: new FormControl('', [Validators.required]),
      });
      this.formBuscarPokemon = new FormGroup({
        buscarPokemon: new FormControl({})
      });
    }

  ngOnInit(): void {
    this.CargarPokemones();
  }

  CargarPokemones() {
    this.cargarPokemones
      .ConsultarPokemones()
      .subscribe((response: any)  => {
        this.dataPokemon = response;
      });
  }

  CargarFormPokemones(){
    this.flagFormNuevo = true;
  }

  NuevoPokemon(){
    this._operacionPokemon.NuevoPokemon(this._datosPokemonInterface).subscribe(data => {
      console.info(data)
      this.dataPokemon.push(data);
      this.LimpiarPokemon();
      this.mensajeEvento = "Operación Exitosa";
    });

  }

  EliminarPokemon(id:any){
    this._operacionPokemon.EliminarPokemon(id).subscribe(data => {
      for (let index = 0; index < this.dataPokemon.length; index++) {
        if (this.dataPokemon[index].id == id) {
          this.dataPokemon.splice(index,1);
        }
      }
    });
  }

  EditarPokemon(id:any){
    this.flagFormNuevo = true;
    this.flagEditar = true;

    for (let index = 0; index < this.dataPokemon.length; index++) {
      if (this.dataPokemon[index].id == id) {
        this._datosPokemonInterface.name = this.dataPokemon[index].name;
        this._datosPokemonInterface.attack = this.dataPokemon[index].attack;
        this._datosPokemonInterface.image = this.dataPokemon[index].image;
        this._datosPokemonInterface.defense = this.dataPokemon[index].defense;
        this.formDatosPokemon.controls['name'].setValue(this.dataPokemon[index].name);
        this.formDatosPokemon.controls['image'].setValue(this.dataPokemon[index].image);
        this.formDatosPokemon.controls['attack'].setValue(this.dataPokemon[index].attack);
        this.formDatosPokemon.controls['defense'].setValue(this.dataPokemon[index].defense);
        this.idEditar = this.dataPokemon[index].id;
      }
    }
  }

  ActualizarPokemon(idEditar : any){
    console.info(idEditar);
    this._datosPokemonInterface.id = idEditar;
    this._operacionPokemon.ActualizarPokemon(this._datosPokemonInterface).subscribe(data => {
      this.mensajeEvento = "Operación Exitosa";
      this.LimpiarPokemon();
     },
     (error) => {
       console.error("error en la consulta");
     });
  }

  LimpiarPokemon(){
    this.flagFormNuevo = false;
    this.mensajeEvento = "";
    this.formDatosPokemon.controls['name'].setValue("");
    this.formDatosPokemon.controls['image'].setValue("");
    this.formDatosPokemon.controls['attack'].setValue("");
    this.formDatosPokemon.controls['defense'].setValue("");
  }


}
