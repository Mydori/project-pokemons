import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { OperacionesPokemonService } from './operaciones-pokemon.service';
import { HttpClient } from "@angular/common/http";
import { RouterTestingModule } from '@angular/router/testing';

describe('OperacionesPokemonService', () => {
  let service: OperacionesPokemonService;
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({

    imports: [ HttpClientTestingModule ],
    providers: [OperacionesPokemonService]
    });
    service = TestBed.inject(OperacionesPokemonService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
