import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpResponse,HttpClientModule } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { interfaceDatosPokemon } from './../Interfaces/datos-pokemon';

@Injectable({
  providedIn: 'root'
})
export class OperacionesPokemonService {
  configUrl = 'https://bp-pokemons.herokuapp.com/';
  _interfaceDatosPokemon: interfaceDatosPokemon[] = [];

  constructor(private http: HttpClient) {}

  ConsultarPokemones(): Observable<interfaceDatosPokemon[]> {
    return this.http.get<interfaceDatosPokemon[]>(this.configUrl+"?idAuthor=1").pipe(
      map((data) => {
        return data;
     }),
      catchError((err) => {
        console.error(err);
        throw err;
      })
    );
  }

  NuevoPokemon(datos:any): Observable<any>{
    return this.http.post(this.configUrl, datos);
  }

  EliminarPokemon(id:any){
    return this.http.delete(this.configUrl+":"+ id);
  }

  ActualizarPokemon(datos: any){
    return this.http.put<any>(this.configUrl+":"+ datos.id, datos);
  }

}
