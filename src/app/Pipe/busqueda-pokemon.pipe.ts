import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'busquedaPokemonPipe'
})
export class BusquedaPokemonPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!args) {
      return value;
    }
    return value.filter((val:any) => {
      let rVal = (val.name.toString()).toLocaleLowerCase().includes(args) || (val.attack.toString()).toLocaleLowerCase().includes(args) ||  (val.defense.toString()).toLocaleLowerCase().includes(args);
      return rVal;
    })
  }

}
