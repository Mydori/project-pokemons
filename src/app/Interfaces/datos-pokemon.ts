export interface interfaceBusquedaPokemon{
  buscarPokemon : string;
}

export interface interfaceDatosPokemon {
  id: string;
  name: string;
  image: string;
  attack: string;
  defense: string;
  hp: string;
  type: string;
  idAuthor: string;
}
